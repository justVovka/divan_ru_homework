<?php

function resize($newWidth, $targetFile, $originalFile)
{
    $info = getimagesize($originalFile);
    $mime = $info['mime'];
    // Определяем тип изображения
    switch ($mime) {
        case 'image/jpeg':
            $image_create_func = 'imagecreatefromjpeg';
            $image_save_func = 'imagejpeg';
            $new_image_ext = 'jpg';
            break;

        case 'image/png':
            $image_create_func = 'imagecreatefrompng';
            $image_save_func = 'imagepng';
            $new_image_ext = 'png';
            break;

        case 'image/gif':
            $image_create_func = 'imagecreatefromgif';
            $image_save_func = 'imagegif';
            $new_image_ext = 'gif';
            break;

        default:
            throw new Exception('Unknown image type.');
    }

    $img = $image_create_func($originalFile);
    list($width, $height) = getimagesize($originalFile);

    $newHeight = ($height / $width) * $newWidth;
    $tmp = imagecreatetruecolor($newWidth, $newHeight);
    // Сохраняем прозрачность
    imagealphablending($tmp, false);
    imagesavealpha($tmp, true);
    $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
    imagefilledrectangle($tmp, 0, 0, $newWidth, $newHeight, $transparent);
    imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

    if (file_exists($targetFile)) {
        unlink($targetFile);
    }
    $image_save_func($tmp, "$targetFile.$new_image_ext");
}
function createWatermark($watermarkSize, $mainImage, $watermarkFile)
{
    resize($watermarkSize, 'watermark', $watermarkFile);
    $animation = $mainImage;
    $watermark = "watermark.png";
    $watermarked_animation = "./images/result.gif";
    $cmd = " $animation -coalesce -gravity South " .
        " -geometry +100+200 null: $watermark -layers composite -layers optimize ";

    exec("convert $cmd $watermarked_animation ");
}

@mkdir("images/", 0777);
if (move_uploaded_file($_FILES['mainImg']['tmp_name'], "images/".basename($_FILES['mainImg']['name'])) &&
    move_uploaded_file($_FILES['wmImg']['tmp_name'], "images/".basename($_FILES['wmImg']['name']))) {

    echo "<div class='alert alert-success' role='alert'>Файлы успешно загружены</div>";
    createWatermark($_POST['wmSize'], $_FILES['mainImg']['name'], $_FILES['wmImg']['name']);
}
$dir = "images/";
$files = scandir($dir);

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Наложение водяного знака на gif файл</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Заполните поля ниже:</h1>
            </div>
            <div class="col-md-12">
                <form method="post" action="<?= $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="file" id="mainImg" name="mainImg">
                        <label for="mainImg">На какое изображение наложим знак?</label>
                    </div>
                    <div class="form-group">
                        <input type="file" id="wmImg" name="wmImg">
                        <label for="wmImg">Выберите водяной знак</label>
                    </div>
                    <div class="form-group">
                        <label for="wmSize">Выберите размер водяного знака:</label>
                        <input type="range" class="custom-range" min="10" max="550" id="wmSize" name="wmSize"
                               onchange="updateTextInput(this.value)";>
                        <input type="text" id="textInput" value="">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Сгенерировать</button>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
            <?php
                foreach($files as $key=>$val)
                {
                    if($val != "." && $val != "..")
                    {
                        $pieces = explode(".", $val);

                            if(strtolower(($pieces[0]) == "result")) {
                                continue;
                            }
                        if(strtolower ($pieces[1])== "jpg" || strtolower ($pieces[1])== "jpeg" ||
                            strtolower ($pieces[1])== "png" || strtolower ($pieces[1])== "gif")
                        {

                        echo '<img src="images/'.$val.'" />';
                        }
                    }
                }

                ?>
            </div>
            <div class="col-md-12">
                <?
                if(glob("images/result.gif")) {
                    echo "<h2>А вот результат:</h2>";
                    echo "<img src='images/result.gif'>";
                }
                ?>
            </div>
        </div>
    </div>
    <script>
        function updateTextInput(val) {
            document.getElementById('textInput').value=val;
        }
    </script>
</body>
</html>

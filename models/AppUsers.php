<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class AppUsers extends ActiveRecord
{
    public function rules()
    {
        return [
            [['user_name', 'user_email', 'user_password'], 'required'],
        ];
    }

    public static function tableName()
    {
        return "yii2_app_users";
    }

    public static function getAll()
    {
        $allUsers = Yii::$app->cache->get('allUsers');
        if(!$allUsers) {
            $allUsers = self::find()->all();
            Yii::$app->cache->set('allUsers', $allUsers, 3600);
        }
        return $allUsers;
    }

    public static function getOne($user_id)
    {
        $user = self::find()->where(['user_id'=>$user_id])->one();

        return $user;
    }
}
<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AppUsers;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionView($user_id)
    {
        $user = AppUsers::getOne($user_id);
        return $this->render('view', ['user'=>$user]);
    }

    public function actionUsers()
    {
        $users = AppUsers::getAll();
        return $this->render('users', ['users'=>$users]);
    }

    public function actionCreate()
    {
        $new_user = new AppUsers();

        if($_POST['AppUsers']) {
            $new_user->user_name = $_POST['AppUsers']['user_name'];
            $new_user->user_email = $_POST['AppUsers']['user_email'];
            $new_user->user_password = $_POST['AppUsers']['user_password'];

            if($new_user->validate() && $new_user->save()) {
                return $this->redirect(['users']);
            }
        }
        return $this->render('create', ['new_user'=>$new_user]);
    }

    public function actionEdit($user_id)
    {

        $user = AppUsers::getOne($user_id);

        if($_POST['AppUsers']) {
            $user->user_name = $_POST['AppUsers']['user_name'];
            $user->user_email = $_POST['AppUsers']['user_email'];
            $user->user_password = $_POST['AppUsers']['user_password'];

            if($user->validate() && $user->save()) {
                return $this->redirect(['users']);
            }
        }
        return $this->render('edit', ['user'=>$user]);
    }

    public function actionDelete($user_id)
    {
        $user = AppUsers::getOne($user_id);
        $user->delete();
        return $this->redirect(['users']);
    }

    public function actionClear()
    {
        Yii::$app->cache->delete('AllUsers');
        return $this->redirect($_SERVER['HTTP_REFERER']);
    }
}

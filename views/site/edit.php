<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редактировать пользователя ' . $user->user_name;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($user, 'user_name')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($user, 'user_email')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($user, 'user_password')->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= Html::submitButton('Сохранить', ['class'=>'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

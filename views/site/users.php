<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Управление пользователями';

?>
<h1><?= Html::encode($this->title) ?></h1>
<!-- TODO Стиль таблицы -->
<table class="table">
    <thead>
    <tr>
        <th>id</th>
        <th>Имя пользователя</th>
        <th>Email пользователя</th>
        <th>Пароль пользователя</th>
        <th>Действия</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($users as $user): ?>
        <tr>
            <td><?= $user->user_id?></td>
            <td><?= $user->user_name?></td>
            <td><?= $user->user_email?></td>
            <td><?= $user->user_password?></td>
            <td>
                <a href="/site/edit/<?= $user->user_id?>"  class="btn btn-primary">Редактировать</a>
                <a href="/site/delete/<?= $user->user_id?>" class="btn btn-danger">Удалить</a>
            </td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
<a href="/site/create" class="btn btn-success mt-5">Создать пользователя</a>
<a href="/site/clear/" class="btn btn-primary">Очистить кэш</a>
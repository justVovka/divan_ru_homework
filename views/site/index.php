<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовая задание</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Пояснения:</h2>

                <p>Чтобы зайти в раздел "Управления пользователями" нажмите на login, введите admin в оба поля.</p>
                <p>Дамп базы данных надодится в дирректирии <i>"db_dump"</i>, расположенной в корне сайта.</p>
                <p>В разработке использовал PHP версии 7.2</p>
            </div>
        </div>

    </div>
</div>

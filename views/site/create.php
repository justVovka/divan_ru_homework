<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Создать пользователя';
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($new_user, 'user_name')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($new_user, 'user_email')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($new_user, 'user_password')->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= Html::submitButton('Создать', ['class'=>'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
